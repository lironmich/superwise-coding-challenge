import React from "react";
import ReactDOM from "react-dom";
import { AppComponent } from "./App";
import * as serviceWorker from "./service-worker";
import { ErrorBoundary } from "./components/error-boundary/error-boundary";
import "./index.scss";

ReactDOM.render(
  <ErrorBoundary>
    <React.StrictMode>
      <AppComponent />
    </React.StrictMode>
  </ErrorBoundary>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
