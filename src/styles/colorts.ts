const COLOR_PALLETE = {
  primary_1: "rgb(28, 45, 74)",
  primary_2: "rgb(220, 251, 252)",
};

export const COLORS = {
  text: {
    light: "white",
    dark: "black",
  },
  dashboard: {
    background: COLOR_PALLETE.primary_1,
  },
  chart: {
    background: COLOR_PALLETE.primary_2,
  },
};
