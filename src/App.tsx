import React from "react";
import styled from "styled-components";
import { ChartComponent } from "./components/chart/chart";
import { DashboardComponent } from "./components/dashboard/dashboard";

const App = styled.div`
  height: 100%;
  display: flex;
`;

const Dashboard = styled.div`
  flex: 0 0 300px;
`;

const Chart = styled.div`
  flex: 1;
`;

function AppComponent() {
  return (
    <App>
      <Dashboard>
        <DashboardComponent />
      </Dashboard>
      <Chart>
        <ChartComponent />
      </Chart>
    </App>
  );
}

export { AppComponent };
