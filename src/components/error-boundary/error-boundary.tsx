import React, { ReactNode } from "react";
import styled from "styled-components";

interface IErrorBoundaryProps {
  children: ReactNode;
}

interface IErrorBoundaryState {
  hasError: boolean;
}

const ErrorMessage = styled.h1`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

class ErrorBoundary extends React.Component<
  IErrorBoundaryProps,
  IErrorBoundaryState
> {
  constructor(props: IErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  render() {
    if (this.state.hasError) {
      return <ErrorMessage>Something went wrong.</ErrorMessage>;
    }

    return this.props.children;
  }
}

export { ErrorBoundary };
