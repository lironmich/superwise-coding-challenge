import React from "react";
import { render, RenderResult } from "@testing-library/react";
import { ChartComponent } from "./chart";

describe("chart", () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    renderResult = render(<ChartComponent />);
  });

  test("should contain correct title", async () => {
    const title = await renderResult.findByText("My Chart");

    expect(title).toBeTruthy();
  });
});
