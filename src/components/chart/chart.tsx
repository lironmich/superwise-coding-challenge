import React from "react";
import styled from "styled-components";
import { COLORS } from "../../styles/colorts";
import { GraphComponent } from "../graph/graph";

const Chart = styled.div`
  height: 100%;
  padding: 10px;
  background-color: ${COLORS.chart.background};
`;

const Title = styled.h1`
  color: ${COLORS.text.dark};
`;

function ChartComponent() {
  return (
    <Chart>
      <Title>My Chart</Title>
      <GraphComponent />
    </Chart>
  );
}

export { ChartComponent };
