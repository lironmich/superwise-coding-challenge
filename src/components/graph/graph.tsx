import React, { useMemo, useState } from "react";
import styled from "styled-components";
import {
  FlexibleWidthXYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  LineSeries,
  Crosshair,
} from "react-vis";
import { GRAPHS } from "./presets/graph-data";
import { FilterComponent } from "./filters/filter";
import { IPoint } from "./interfaces/IPoint";
import {
  FILTER_BY_COUNT_DEFAULT,
  FILTER_BY_GRAPH_DEFAULT,
  FILTER_BY_COUNT_OPTIONS,
  FILTER_BY_GRAPH_OPTIONS,
} from "./graph-filter-defaults";
import { BarComponent } from "../bar/bar";

const Graph = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const FilterTimes = styled.div``;

const BarContainer = styled.div`
  width: 30%;
`;

const Filters = styled.div`
  width: 100%;
  display: flex;
`;

const RowMargin = styled.div`
  flex: 0 0 30px;
`;

const FilterMargin = styled.div`
  flex: 0 0 10px;
`;

const getFilteredGraph = (
  filterByCount: number,
  displayedGraph: number | "all"
) => {
  const startTime = Date.now();

  const graphs = JSON.parse(JSON.stringify(GRAPHS));

  const displayedGraphs =
    displayedGraph === "all" ? graphs : [graphs[displayedGraph]];

  displayedGraphs.forEach((graph: IPoint[]) => graph.splice(filterByCount));

  const millisecondsPassed = Date.now() - startTime;

  return [displayedGraphs, millisecondsPassed];
};

function GraphComponent() {
  const [selectedValues, setSelectedValues] = useState<IPoint[]>([]);

  const [filterByCount, setFilterByCount] = useState<number>(
    FILTER_BY_COUNT_DEFAULT
  );

  const [displayedGraph, setDisplayedGraph] = useState<number | "all">(
    FILTER_BY_GRAPH_DEFAULT
  );

  const [filteredGraph, millisecondsPassed] = useMemo(
    () => getFilteredGraph(filterByCount, displayedGraph),
    [filterByCount, displayedGraph]
  );

  const onMouseLeave = () => {
    setSelectedValues([]);
  };

  const onNearestX = (value: any, { index }: { index: number }) => {
    setSelectedValues(GRAPHS.map((data) => data[index]));
  };

  const onFilterByCountChange = (value: number) => {
    setFilterByCount(value);
  };

  const onDisplayedGraphChange = (value: number | "all") => {
    setDisplayedGraph(value);
  };

  return (
    <Graph>
      <RowMargin />

      <FilterTimes>
        Time it took to filter graph: {millisecondsPassed} milliseconds{" "}
      </FilterTimes>

      <RowMargin />

      <BarContainer>
        <BarComponent percentage="33%" />
      </BarContainer>

      <RowMargin />

      <Filters>
        <FilterComponent
          options={FILTER_BY_COUNT_OPTIONS}
          initialOption={FILTER_BY_COUNT_DEFAULT}
          onChange={onFilterByCountChange}
          displayedOptionLabel="items"
        />

        <FilterMargin />

        <FilterComponent
          options={FILTER_BY_GRAPH_OPTIONS}
          initialOption={FILTER_BY_GRAPH_DEFAULT}
          onChange={onDisplayedGraphChange}
          displayedOptionLabel="displayed"
        />
      </Filters>

      <RowMargin />

      <FlexibleWidthXYPlot onMouseLeave={onMouseLeave} height={300}>
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis />
        <YAxis />

        {filteredGraph.map((value: any, index: number) => {
          if (index === 0) {
            return (
              <LineSeries
                key={index}
                onNearestX={onNearestX}
                data={filteredGraph[0]}
              />
            );
          }

          return <LineSeries key={index} data={filteredGraph[index]} />;
        })}

        <Crosshair values={selectedValues} />
      </FlexibleWidthXYPlot>
    </Graph>
  );
}

export { GraphComponent };
