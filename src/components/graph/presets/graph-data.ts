import Data from "./data.json";
import { IPoint } from "../interfaces/IPoint";

interface IRawPoint {
  _id: string;
  segmentA: number;
  segmentB: number;
  segmentC: number;
  segmentD: number;
  date: string;
}

const orderedPoints = Data.sort((p1: IRawPoint, p2: IRawPoint) => {
  return (
    new Date(p1.date).getMilliseconds() - new Date(p2.date).getMilliseconds()
  );
});

const segmentA: IPoint[] = orderedPoints.map((point, index) => ({
  x: index,
  y: point.segmentA,
}));

const segmentB: IPoint[] = orderedPoints.map((point, index) => ({
  x: index,
  y: point.segmentB,
}));

const segmentC: IPoint[] = orderedPoints.map((point, index) => ({
  x: index,
  y: point.segmentC,
}));

const segmentD: IPoint[] = orderedPoints.map((point, index) => ({
  x: index,
  y: point.segmentD,
}));

export const GRAPHS = [segmentA, segmentB, segmentC, segmentD];
