import React, { useState } from "react";
import styled from "styled-components";

interface IFilterProps {
  options: any[];
  initialOption: any;
  onChange: (value: any) => void;
  displayedOptionLabel?: string;
}

const Filter = styled.div`
  z-index: 2;
  text-align: center;
  position: relative;
`;

const OptionsContainer = styled.div<{ isOpen: boolean }>`
  position: absolute;
  overflow: hidden;
  max-height: ${(props) => (props.isOpen ? "200px" : 0)};
  transition: all 0.2s ease-out;
`;

const Option = styled.div<{ isSelected?: boolean }>`
  min-width: 150px;
  padding: 10px;
  border-radius: 10px;
  border: 1px solid black;
  background: white;
  cursor: pointer;
  font-weight: ${(props) => (props.isSelected ? "bold" : "normal")};
`;

function FilterComponent(props: IFilterProps) {
  const [selected, setSelected] = useState<any>(props.initialOption);
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const setSelectetOption = (value: any) => {
    setSelected(value);

    props.onChange(value);

    toggleOptions();
  };

  const renderOptions = () => {
    return props.options.map((value, index) => (
      <Option
        key={index}
        isSelected={value === selected}
        onClick={() => setSelectetOption(value)}
      >
        {value}
      </Option>
    ));
  };

  const toggleOptions = () => {
    setIsOpen(!isOpen);
  };

  return (
    <Filter>
      <Option onClick={() => toggleOptions()}>
        {selected} {props.displayedOptionLabel}
      </Option>

      <OptionsContainer isOpen={isOpen}>{renderOptions()}</OptionsContainer>
    </Filter>
  );
}

export { FilterComponent };
