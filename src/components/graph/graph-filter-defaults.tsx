export const FILTER_BY_COUNT_OPTIONS = [10, 20, 30, 50, 100, 150];
export const FILTER_BY_COUNT_DEFAULT = 100;
export const FILTER_BY_GRAPH_OPTIONS: (number | "all")[] = [0, 1, 2, 3, "all"];
export const FILTER_BY_GRAPH_DEFAULT = "all";
