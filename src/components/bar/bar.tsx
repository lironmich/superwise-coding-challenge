import React from "react";
import styled from "styled-components";

const BarContainer = styled.div`
  width: 100%;
  height: 20px;
  display: flex;
  background-color: white;
  justify-content: flex-start;
`;

const Bar = styled.div<{ percentage: string }>`
  border-bottom: 20px solid orange;
  height: 0;
  width: ${(props) => props.percentage};
`;

interface IBarProps {
  percentage: string;
}

function BarComponent(props: IBarProps) {
  return (
    <BarContainer>
      <Bar percentage={props.percentage} />
    </BarContainer>
  );
}

export { BarComponent };
