import React from "react";
import styled from "styled-components";
import { COLORS } from "../../styles/colorts";

const Dashboard = styled.h1`
  text-align: center;
  height: 100%;
  padding: 10px;
  color: ${COLORS.text.light};
  background-color: ${COLORS.dashboard.background};
`;

function DashboardComponent() {
  return <Dashboard>My Dashboard</Dashboard>;
}

export { DashboardComponent };
