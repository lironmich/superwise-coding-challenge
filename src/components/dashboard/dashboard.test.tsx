import React from "react";
import { render, RenderResult } from "@testing-library/react";
import { DashboardComponent } from "./dashboard";

describe("chart", () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    renderResult = render(<DashboardComponent />);
  });

  test("should contain correct title", async () => {
    const title = await renderResult.findByText("My Dashboard");

    expect(title).toBeTruthy();
  });
});
